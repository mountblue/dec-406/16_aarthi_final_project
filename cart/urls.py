from django.urls import path
from cart import views

urlpatterns = [
    path('cart/', views.get_cart_item, name='cart'),
    path('add/<photo_id>', views.add_cart_items, name='add'),
    path('status/', views.buy_cart_items, name='buy_cart_items'),
    path('delete/<cart_id>/', views.delete_cart_items, name='delete_cart_items'),
    path('orders/', views.view_orders, name='view_orders'),
    path('orders/<order_id>/', views.view_order_details, name='view_order_details'),
]
