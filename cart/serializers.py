from .models import Cart, CartItem, Order
from buyer.models import Buyer
from photographer.models import Photographer, Photo, PhotoRating
from rest_framework import serializers

class CartSerializer(serializers.HyperlinkedModelSerializer):
    buyer = serializers.HyperlinkedRelatedField( many=False, view_name='buyer-detail', lookup_url_kwarg = 'pk', read_only=True)
    class Meta:
        model = Cart
        fields = ('buyer', 'payment-status')


class CartItemSerializer(serializers.HyperlinkedModelSerializer):
    photo = serializers.HyperlinkedRelatedField( many=False, view_name='photo-detail', lookup_url_kwarg = 'pk', read_only=True)
    cart = serializers.HyperlinkedRelatedField( many=False, view_name='cart-detail', lookup_url_kwarg = 'pk', read_only=True)
    
    class Meta:
        model = CartItem
        fields = ('photo', 'cart')

class OrderSerializer(serializers.HyperlinkedModelSerializer):
    photo = serializers.HyperlinkedRelatedField( many=False, view_name='photo-detail', lookup_url_kwarg = 'pk', read_only=True)
    cart = serializers.HyperlinkedRelatedField( many=False, view_name='cart-detail', lookup_url_kwarg = 'pk', read_only=True)
    
    class Meta:
        model = Order
        fields = ('photo', 'cart')