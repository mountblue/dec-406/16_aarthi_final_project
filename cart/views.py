from django.http import request, HttpResponse
from .models import Orders, Cart, Buyer, Photo
from django.shortcuts import render, redirect
from buyer.views import home
from django.contrib import messages


def view_orders(request):
    buyer_id = 1
    buyer = Buyer.objects.filter(pk=buyer_id)
    order_list = Orders.objects.filter(cart_status='S', buyer=buyer[0])
    return render(request, 'view_orders.html', {'order_list': order_list})


def view_order_details(request, order_id):
    order_details = Cart.objects.filter(order_id=order_id)
    return render(request, 'view_order_details.html', {'order_details': order_details})


def get_cart_item(request):
    buyer_id = 1
    buyer = Buyer.objects.filter(pk=buyer_id)
    order = Orders.objects.filter(cart_status='A', buyer=buyer[0])
    if order:
        cart_items = Cart.objects.filter(order_id=order[0])
        total_price = 0
        for item in cart_items:
            total_price += item.photo.price
        tax = total_price*(1/100)
        sub_total = total_price+tax
        context = {
            'cart_items': cart_items,
            'total_price': total_price,
            'tax': tax,
            'sub_total': sub_total
        }
    else:
        messages.warning(request, f'No active cart items. Add items to proceed')
    return render(request, 'cart.html', context)


def buy_cart_items(request):
    buyer_id = 1
    buyer = Buyer.objects.filter(pk=buyer_id)
    order = Orders.objects.filter(cart_status='A', buyer=buyer[0])
    cart = Cart.objects.filter(order_id=order[0])
    if request.method == 'POST' and cart:
        Orders.objects.filter(cart_status='A', buyer=buyer[0]).update(cart_status='S')
        order = Orders(cart_status='A', buyer=buyer[0])
        order.save()
        return redirect('view_orders')
    else:
        messages.warning(request, 'No active cart items. Add items to proceed')
        return redirect('cart')


def delete_cart_items(request, cart_id):
    Cart.objects.filter(pk=cart_id).delete()
    messages.success(request, "Item deleted successfully")
    return redirect('cart')


def add_cart_items(request, photo_id):
    buyer_id = 1
    buyer = Buyer.objects.filter(pk=buyer_id)
    order = Orders.objects.filter(cart_status='A', buyer=buyer[0])
    print(photo_id)
    photo = Photo.objects.filter(pk=photo_id)
    if request.method == 'GET':
        cart_item = Cart(order_id=order[0], photo=photo[0])
        cart_item.save()
        messages.success(request, "Item added successfully")
    return redirect('homepage')
