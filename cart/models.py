from django.contrib.auth.models import User
from django.db import models
from buyer.models import Buyer
from photographer.models import Photographer, Photo


class Orders(models.Model):
	buyer = models.ForeignKey(Buyer, on_delete=models.CASCADE)
	cart_status = models.CharField(max_length=1, choices=(('S', 'Success'), ('A', 'Active')))


class Cart(models.Model):
	order_id = models.ForeignKey(Orders, on_delete=models.CASCADE)
	photo = models.ForeignKey(Photo, on_delete=models.CASCADE)
