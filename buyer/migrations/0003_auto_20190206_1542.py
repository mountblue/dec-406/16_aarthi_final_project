# Generated by Django 2.1.5 on 2019-02-06 15:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('buyer', '0002_auto_20190206_1511'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buyer',
            name='user_photo',
            field=models.ImageField(blank=True, default='images/photographers/avatar.png', upload_to=''),
        ),
    ]
