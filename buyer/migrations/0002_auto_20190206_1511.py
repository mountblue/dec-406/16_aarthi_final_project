# Generated by Django 2.1.5 on 2019-02-06 15:11

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('buyer', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='buyer',
            name='email',
            field=models.EmailField(blank=True, max_length=70, null=True, unique=True),
        ),
        migrations.AddField(
            model_name='buyer',
            name='name',
            field=models.CharField(default=-1.0, max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='buyer',
            name='mobile_no',
            field=models.CharField(blank=True, max_length=17, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.", regex='^\\+?1?\\d{9,15}$')]),
        ),
    ]
