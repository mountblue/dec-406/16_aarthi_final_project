from .models import Buyer
from rest_framework import serializers


class BuyerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Buyer
        fields = ('username','email','geneder','mobile_no')