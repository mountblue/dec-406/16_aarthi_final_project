from django.contrib.auth.models import User
from django.db import models
from django.core.validators import RegexValidator


class Buyer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    email = models.EmailField(max_length=70, null=True, blank=True, unique=True)
    gender = models.CharField(max_length=1, choices= (('M', 'Male'),('F', 'Female')))
    user_photo = models.ImageField(blank=True, default='images/photographers/avatar.png')
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    mobile_no = models.CharField(validators=[phone_regex], max_length=17, blank=True) # validators should be a list
