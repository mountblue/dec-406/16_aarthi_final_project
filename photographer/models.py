from django.contrib.auth.models import User
from buyer.models import Buyer
from django.db import models
from django.core.validators import RegexValidator


class Photographer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField(max_length=70, null=True, blank=True, unique=True)
    gender = models.CharField(max_length=1, choices= (('M', 'Male'),('F', 'Female')))
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    mobile_no = models.CharField(validators=[phone_regex], max_length=17, blank=True) # validators should be a list
    photographer_photo = models.ImageField(blank=True, default='images/photographers/avatar.png')
    paypal_id = models.CharField(max_length = 16)
    experience = models.FloatField()


class Photo(models.Model):
    photographer = models.ForeignKey(Photographer, on_delete=models.CASCADE)
    url = models.URLField()
    date = models.DateField(auto_now_add=True, auto_now=False)
    price = models.FloatField()
    title = models.CharField(max_length=30)
    description = models.TextField()
    likes = models.IntegerField()
    thumbnail_path = models.URLField()


class PhotoRating(models.Model):
    buyer = models.ForeignKey(Buyer, on_delete=models.CASCADE)
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE)
    upvotes = models.IntegerField()
    downvotes = models.IntegerField()
