from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from PIL import ImageFile
from celery import Celery
import uuid
from .models import Photo, Photographer
import boto3

s3 = boto3.client(
    's3',
    aws_access_key_id='XXXXXXXXXXX',
    aws_secret_access_key='XXXXXXXXXXXXXXXXXX'
)

celery_app = Celery('photo_upload', backend='redis://localhost', broker='redis://localhost')


@celery_app.task
def watermark_text(f, price, title, desc, likes):
    input_image_path = 'static/' + str(f)
    output_image_path = 'static/thumbnails/' + str(f)
    ImageFile.LOAD_TRUNCATED_IMAGES = True
    print('watermark_text', input_image_path)
    size = 250, 250
    photo = Image.open(input_image_path)
    color = (166, 212, 236)
    photo.thumbnail(size)
    width, height = photo.size
    pos = ((width / 2)-40, height / 2)
    drawing = ImageDraw.Draw(photo)
    text = 'PHOTO BOOTH'
    font = ImageFont.truetype("arial.ttf", size=10)
    drawing.text((0, 0), text, fill=color, font=font)
    drawing.text((0, height-20), text, fill=color, font=font)
    drawing.text((width-75,0), text, fill=color, font=font)
    drawing.text((width-75, height-20), text, fill=color, font=font)
    drawing.text((height, width), text, fill=color, font=font)
    drawing.text(pos, text, fill=color, font=font)
    photo.save(output_image_path)
    bucket_name = 'my-photobooth-aws'
    s3.upload_file(input_image_path, bucket_name, f)
    img_url = 'https://s3.ap-south-1.amazonaws.com/' + bucket_name + '/' + f
    bucket_name = 'my-photobooth-aws-thumbnail'
    f = uuid.uuid4()
    s3.upload_file(output_image_path, bucket_name, str(f)+'.jpg')
    img_thumbnail_url = 'https://s3.ap-south-1.amazonaws.com/'+bucket_name+'/'+str(f)+'.jpg'
    upload_image_details(img_url, img_thumbnail_url, price, title, desc, likes)


@celery_app.task
def upload_image_details(real_img_url, thumbnail_path, price, title, desc, likes):
    photographer_id = 2
    photographer = Photographer.objects.filter(pk=photographer_id)
    photo_details = Photo(photographer=photographer[0], url=real_img_url, price=price, title=title, description=desc,
                          likes=likes, thumbnail_path=thumbnail_path)
    photo_details.save()


@celery_app.task
def delete_image_details(img_url, img_thumbanil_url):
    session = boto3.Session(
        aws_access_key_id="XXXXXXXXXXXX",
        aws_secret_access_key="XXXXXXXXXXXXXXXXX",
        region_name="ap-south-1"
    )
    s3 = session.resource("s3")
    bucket_name = 'my-photobooth-aws'
    obj = s3.Object(bucket_name, img_url.split('/')[-1])
    obj.delete()
    bucket_name = 'my-photobooth-aws-thumbnail'
    obj = s3.Object(bucket_name, img_thumbanil_url.split('/')[-1])
    obj.delete()


