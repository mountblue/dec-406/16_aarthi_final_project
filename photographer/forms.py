from django import forms


class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50)
    file = forms.FileField()
    price = forms.IntegerField()
    desc = forms.CharField(widget=forms.Textarea, min_length=1)


class EditFileForm(forms.Form):
    title = forms.CharField(max_length=50)
    price = forms.IntegerField()
    desc = forms.CharField(widget=forms.Textarea, min_length=1)
