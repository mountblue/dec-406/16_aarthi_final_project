from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import UploadFileForm, EditFileForm
import boto3
import os
import uuid
from photographer.celery_tasks import watermark_text, upload_image_details, delete_image_details
from .models import Photo, Photographer
from django.contrib import messages

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'proj.settings')


s3 = boto3.client(
    's3',
    aws_access_key_id='XXXXXXXXXXXXXXXX',
    aws_secret_access_key='XXXXXXXXXXXXXXXXXX'
)


def photo_home(request):
    photographer_id = 2
    photographer = Photographer.objects.filter(pk=photographer_id)
    photos = Photo.objects.filter(photographer=photographer[0])
    context = {
        'images_url': photos,
        'photographer': photographer[0],
    }
    return render(request, 'photographer_profile_page.html', context)


def upload(request):
    if request.method == 'POST':
        print("method")
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            print("validated")
            title = form.cleaned_data['title']
            price = form.cleaned_data['price']
            desc = form.cleaned_data['desc']
            likes = 0
            id = uuid.uuid4()
            handle_uploaded_file(request.FILES['file'], str(id))
            watermark_text.delay(str(id)+'.jpg', price, title, desc, likes)
            messages.success(request, 'image uploaded')
        return redirect('/photographer')
    else:
        form = UploadFileForm()
    return render(request, 'upload_form.html', {'form': form})


def handle_uploaded_file(f, fname):
    with open('static/'+fname+'.jpg', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def edit(request, photo_id):
    if request.method == 'POST':
        print("method")
        form = EditFileForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            price = form.cleaned_data['price']
            desc = form.cleaned_data['desc']
            Photo.objects.filter(pk=int(photo_id)).update(title=title, price=price,description=desc)
            messages.success(request, 'image edited')
            return redirect('/photographer')
    else:
        photo = Photo.objects.filter(pk=int(photo_id))
        form = EditFileForm(request.POST)
        context = {
            'form': form,
            'photo': photo[0],
        }
        return render(request, 'edit_form.html', context)


def delete(request, photo_id):
    photo = Photo.objects.filter(pk=int(photo_id))
    delete_image_details.delay(photo[0].url, photo[0].thumbnail_path)
    Photo.objects.filter(pk=int(photo_id)).delete()
    messages.warning(request, 'image deleted')
    return redirect('/photographer')
