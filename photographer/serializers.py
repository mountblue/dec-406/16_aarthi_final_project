from .models import Photo
from rest_framework import serializers


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = ('photographer', 'url', 'date', 'price', 'title', 'category', 'description', 
    			'likes', 'thumbnail_path')