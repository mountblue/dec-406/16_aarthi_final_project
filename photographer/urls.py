from django.urls import path
from . import views

urlpatterns = [
    path('', views.photo_home, name='photo_home'),
    path('upload/', views.upload, name='upload'),
    path('edit/<photo_id>/', views.edit, name='edit_photo'),
    path('delete_photo/<photo_id>/', views.delete, name='delete_photo'),
]